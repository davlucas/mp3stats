#!/usr/bin/env python

import eyed3
import os
import collections
import numpy as np
import datetime
eyed3.log.setLevel("CRITICAL")

contok=""
methok=""


while contok is not 'ok':
    dir=input("Directory to analyze " + '\n' + '->' + '\t')
    try:
        audofile=os.listdir(dir)
        if dir[-1:] is not '/':
            dir= dir + '/'
        contok='ok'
    except:
        print ("Not such directory : " + '\t'+ dir)





while methok is not 'ok':
    analyzetype=input('\n' + "Method of analyze:" +'\n' +"A- Artist by number of songs" + '\n' + "B- Artist by total duration" + '\n' + "C - Song by duration" + '\n' + "D- Songs without artist , or title " + '\n'+ "->" + '\t')
    analyzetype=analyzetype.lower()
    if analyzetype=='a' or analyzetype=='b' or analyzetype=='c' or analyzetype=='d':
        methok='ok'
    else:
        print("Non acceptable value:" + '\t' + analyzetype)



audiofile=[dir + song for song in audofile]








if analyzetype == 'a':
    artistoccurence=[]
    for song in audiofile:
        try:
            media=eyed3.load(song)
            if media.tag.artist:
                artistoccurence.append(media.tag.artist)
            if not media.tag.artist:
                artistoccurence.append("unknown")
        except:
            print(song + " is not a mp3 file")
    artistocc=[[artistoccurence.count(artist), artist] for artist in set(artistoccurence)]
    artistocc.sort()
    total=0
    total=len(artistocc)
    for element in artistocc:
        pct="{:.2f}".format((100*element[0])/total)
        print (str(element[1]) + ' \t ' + str(element[0]) + ' songs ' +'\t'+ str(pct) +  '%' )
    print ("Number of mp3 songs:" + '\t' + str(total))







elif analyzetype == 'b':
    songduration={}
    couple=[]
    duration=dict()
    listdur=[]
    for song in audiofile:
        try:
            media=eyed3.load(song)
            if media.tag.artist:
                couple.append([media.tag.artist, media.info.time_secs ])
            if not media.tag.artist:
                couple.append(["unknown", media.info.time_secs ])
        except:
            print(song + " is not a mp3 file")
    dureetotale=0
    for element in couple:
        dureetotale+=element[1]
        if element[0] in duration:
            duration[element[0]] += element[1]
        else:
            duration[element[0]] = element[1]
    for key,value in duration.items():
        temp=[int(value),key]
        listdur.append(temp)
    print (listdur)
    listdur.sort()
    total=len(listdur)
    for element in listdur:
        pct="{:.2f}".format(100*element[0]/dureetotale)
        print (str(element[1]) + '\t ' + str(datetime.timedelta(seconds=int(element[0]))) + ' \t ' + str(pct) +  '%')
    print ('Durée totale :' + '\t\t' + str(datetime.timedelta(seconds=int(dureetotale))) +'seconds')

elif analyzetype == 'c':
    triple=[]
    for song in audiofile:
        try:
            media=eyed3.load(song)
            if media.tag.artist:
                if media.tag.title:
                    triple.append([media.info.time_secs, media.tag.title, media.tag.artist])
                if not media.tag.title:
                    triple.append([media.info.time_secs, "unknown", media.tag.artist])
            if not media.tag.artist:
                triple.append([media.info.time_secs,media.tag.title,"unknown"])
        except:
            print(song + " is not a mp3 file")
    print (triple)
    triple.sort()
    for element in triple:
        print (str(element[1]) + ' \t ' + str(element[2]) +'\t'+ str(datetime.timedelta(seconds=int(element[0]))))


elif analyzetype == 'd':
    for song in audiofile:
        try:
            media=eyed3.load(song)
            if not media.tag.artist:
                print ('No artist:  ' + song)
            if not media.tag.title:
                print ('no title: ' + song )
        except:
            print(song + " is not a mp3 file")
