#!/usr/bin/env python
"""

Give stats on mp3 dirs
"""
from tkinter import Tk, Button, Label, Entry, Frame, StringVar, LabelFrame, Listbox
from tkinter.messagebox import showinfo
from tkinter import filedialog
from tkinter.colorchooser import askcolor
import os
import datetime
import eyed3
eyed3.log.setLevel("CRITICAL")

binpath = os.path.dirname(os.path.realpath(__file__))

try:
    with open(binpath + '/colors.conf', "r") as conffile:
        COLORS = conffile.readlines()
        COLORS = list(map(lambda s: s.strip(), COLORS))
        (bg, fg) = COLORS
except:
    with open(binpath + '/colors.conf', "w") as f:
        f.write('bisque\nmaroon')
    with open(binpath + '/colors.conf', "r") as conffile:
        COLORS = conffile.readlines()
        COLORS = list(map(lambda s: s.strip(), COLORS))
        (bg, fg) = COLORS

def browse():
    DIRECTORY.pack_forget()
    DIR = filedialog.askdirectory()
    DIRECTORY.insert("end", DIR)
def fillmp():
    listsing = []
    mpdir = DIRECTORY.get()
    if mpdir[-1:] is not '/':
        mpdir = mpdir + '/'
    try:
        audofile = os.listdir(mpdir)
    except:
        showinfo("Not existing directory", "Please, fill a correct directory")
    listsing = [mpdir + song for song in audofile]
    return(listsing)
def changecolor():
    Labelbgcolor = Label(CHANGECOLORS, text="Main Colors")
    Labelbgcolor.grid(column=0, row=0)
    def changebg():
        (a, b) = askcolor(color=bg, title='Choose...')
        with open(binpath + '/colors.conf', "r") as readfile:
            COLORS = readfile.readlines()
            COLORS = list(map(lambda s: s.strip(), COLORS))
        NEWCOLORS = [str(b+'\n'), str(COLORS[1])]
        with open(binpath + '/colors.conf', "w") as writefile:
            writefile.writelines(NEWCOLORS)
        os.execv(sys.executable, [sys.executable] + sys.argv)

    def changefg():
        (a, b) = askcolor(color=fg, title='Choose...')
        with open(binpath + '/colors.conf', "r") as readfile:
            COLORS = readfile.readlines()
            COLORS = list(map(lambda s: s.strip(), COLORS))
        NEWCOLORS = [str(COLORS[0]+'\n'), str(b)]
        with open(binpath + '/colors.conf', "w") as writefile:
            writefile.writelines(NEWCOLORS)
        os.execv(sys.executable, [sys.executable] + sys.argv)
    def reverse():
        revlines = []
        with open(binpath + '/colors.conf', "r") as readfile:
            COLORS = readfile.readlines()
            COLORS = list(map(lambda s: s.strip(), COLORS))
        NEWCOLORS = [str(COLORS[1]+'\n'), str(COLORS[0])]
        with open(binpath + '/colors.conf', "w") as writefile:
            writefile.writelines(NEWCOLORS)
        os.execv(sys.executable, [sys.executable] + sys.argv)

    Buttonbg = Button(CHANGECOLORS, text=bg, fg='black', bg=bg, command=changebg)
    Buttonbg.grid(column=0, row=1)
    Buttonfg = Button(CHANGECOLORS, text=fg, fg='black', bg=fg, command=changefg)
    Buttonfg.grid(column=0, row=2)
    Buttonrev = Button(CHANGECOLORS, text='REVERSE', command=reverse)
    Buttonrev.grid(column=0, row=3)
    CHANGECOLORS.grid(column=1, row=1)


def freq():
    def create_csv():
        file = filedialog.asksaveasfilename()
        with open(file, "w") as csvfile:
            csvfile.write("artist;number of song;percentage on total songs\n")
            for element in artistocc:
                pct = "{:.2f}".format((100*element[0])/total)
                csvfile.write(" {};{};{}\n".format(element[1], element[0], pct))
        showinfo("Sucess", "file Created : {}/artistfrequency.csv".format(binpath))

    LISTSONGDUR.pack_forget()
    LISTDUR.pack_forget()
    ODDSONGS.pack_forget()
    SONGSFREQ.pack_forget()
    LISTFREQ.pack_forget()
    DOUBLELIST.pack_forget()
    listsing = fillmp()
    artistoccurence = []
    artistdict = {}
    BUTTON_CSV = Button(RESULTWINDOW, text='Save as csv file', command=create_csv)
    ButtonDB = Button(RESULTWINDOW, text='DB(NI)')
    for song in listsing:
        os.system('cls' if os.name == 'nt' else 'clear')
        percentage = int(listsing.index(song)*100/len(listsing))
        print(percentage)
        try:
            media = eyed3.load(song)
            artist = str(media.tag.artist).title()
            if artist:
                artistoccurence.append(artist)
            if not media.tag.artist:
                artistoccurence.append("unknown")
        except:
            pass
    artistocc = [[artistoccurence.count(artist), artist] for artist in set(artistoccurence)]
    artistocc.sort(reverse=True)
    total = len(artistoccurence)
    LISTFREQ.grid(column=0, row=0, columnspan=2)
    LISTFREQ.insert("end", "total of song          " + str(total))
    for element in artistocc:
        pct = "{:.2f}".format((100*element[0])/total)
        LISTFREQ.insert("end", " {} :  {}  songs  : {}%".format(element[1], element[0], pct))
    BUTTON_CSV.grid(column=0, row=1)
    ButtonDB.grid(column=1, row=1)
    RESULTWINDOW.grid(column=0, row=3)

def songsingfreq():
    LISTSONGDUR.pack_forget()
    LISTDUR.pack_forget()
    ODDSONGS.pack_forget()
    SONGSFREQ.pack_forget()
    LISTFREQ.pack_forget()
    DOUBLELIST.pack_forget()
    listsing = fillmp()
    songsingoccurence = {}
    couple = []
    occurence = dict()
    listocc = []
    for song in listsing:
        os.system('cls' if os.name == 'nt' else 'clear')
        percentage = int(listsing.index(song)*100/len(listsing))
        print(str(percentage) + '      %')
        try:
            media = eyed3.load(song)
            artist = str(media.tag.artist).title()
            title = str(media.tag.title).title()
            couple.append(artist + ' : ' + title)
        except:
            pass
    for element in couple:
        if element in occurence:
            occurence[element] += 1
        else:
            occurence[element] = 1
    for key, value in occurence.items():
        temp = [int(value), key]
        listocc.append(temp)
    listocc.sort(reverse=True)
    DOUBLELIST.pack()
    for element in listocc:
        if int(element[0]) > 1:
            DOUBLELIST.insert("end", str(element[1]) + ': ' + str(element[0]))
    if int(listocc[0][0]) < 2:
        DOUBLELIST.insert("end", "No found DOUBLE songs ")
    RESULTWINDOW.grid(column=0, row=2)


def SONGFREQ():

    LISTSONGDUR.pack_forget()
    LISTDUR.pack_forget()
    ODDSONGS.pack_forget()
    SONGSFREQ.pack_forget()
    LISTFREQ.pack_forget()
    DOUBLELIST.pack_forget()
    listsing = fillmp()
    titleoccurence = []
    for song in listsing:
        os.system('cls' if os.name == 'nt' else 'clear')
        percentage = int(listsing.index(song)*100/len(listsing))
        print(str(percentage) + '      %')
        try:
            media = eyed3.load(song)
            title = str(media.tag.title).title()
            if title:
                titleoccurence.append(title)
            if not title:
                titleoccurence.append("unknown")
        except:
            pass
    titleocc = [[titleoccurence.count(title), title] for title in set(titleoccurence)]
    titleocc.sort(reverse=True)
    SONGSFREQ.pack()
    for element in titleocc:
        SONGSFREQ.insert("end", "{} : {}  times ".format(element[1], element[0]))
    RESULTWINDOW.grid(column=0, row=3)

########################################################################

##########################################################################
def dur():

    def create_csv():
        file = filedialog.asksaveasfilename()
        with open(file, "w") as csvfile:
            csvfile.write("artist;Length of songs;percentage on total duration\n")
            for element in listdur:
                pct = "{:.2f}".format(100*element[0]/dureetotale)
                duree = datetime.timedelta(seconds=int(element[0]))
                csvfile.write("{};{};{}\n".format(element[1], duree, pct))
        showinfo("Sucess", "file Created : {}/artistduration.csv".format(binpath))
    LISTSONGDUR.pack_forget()
    LISTDUR.pack_forget()
    ODDSONGS.pack_forget()
    SONGSFREQ.pack_forget()
    LISTFREQ.pack_forget()
    DOUBLELIST.pack_forget()
    listsing = fillmp()
    LISTSONGDURation = {}
    couple = []
    duration = dict()
    listdur = []
    BUTTON_CSV = Button(RESULTWINDOW, text='Save as csv file', command=create_csv)
    ButtonDB = Button(RESULTWINDOW, text='DB(NI)')

    for song in listsing:
        os.system('cls' if os.name == 'nt' else 'clear')
        percentage = int(listsing.index(song)*100/len(listsing))
        print(str(percentage) + '      %')
        try:
            media = eyed3.load(song)
            artist = str(media.tag.artist).title()
            if media.tag.artist:
                couple.append([artist, media.info.time_secs])
            if not media.tag.artist:
                couple.append(["unknown", media.info.time_secs])
        except:
            pass
    dureetotale = 0
    for element in couple:
        dureetotale += element[1]
        if element[0] in duration:
            duration[element[0]] += element[1]
        else:
            duration[element[0]] = element[1]
    for key, value in duration.items():
        temp = [int(value), key]
        listdur.append(temp)
    listdur.sort(reverse=True)
    LISTDUR.grid(column=0, row=0, columnspan=2)
    LISTDUR.insert("end", 'Durée totale :' + '        ' + str(datetime.timedelta(seconds=int(dureetotale))))
    for element in listdur:
        pct = "{:.2f}".format(100*element[0]/dureetotale)
        duree = datetime.timedelta(seconds=int(element[0]))
        LISTDUR.insert("end", "{}: {} :  {} %".format(element[1], duree, pct))
    BUTTON_CSV.grid(column=0, row=1)
    ButtonDB.grid(column=1, row=1)
    RESULTWINDOW.grid(column=0, row=2)
###########################################



######################
def SONGDUR():
    def create_csv():
        file = filedialog.asksaveasfilename()
        with open(file, "w") as csvfile:
            csvfile.write("singer:song;Length of songs\n")
            for element in triple:
                duree = datetime.timedelta(seconds=int(element[0]))
                csvfile.write("{};{};{}\n".format(element[1], element[2], duree))
        showinfo("Sucess", "file Created : {}/SONGDURation.csv".format(binpath))
    LISTSONGDUR.pack_forget()
    LISTDUR.pack_forget()
    ODDSONGS.pack_forget()
    SONGSFREQ.pack_forget()
    LISTFREQ.pack_forget()
    DOUBLELIST.pack_forget()
    listsing = fillmp()
    triple = []
    BUTTON_CSV = Button(RESULTWINDOW, text='Save as csv file', command=create_csv)
    ButtonDB = Button(RESULTWINDOW, text='DB(NI)')
    for song in listsing:
        os.system('cls' if os.name == 'nt' else 'clear')
        percentage = int(listsing.index(song)*100/len(listsing))
        print(str(percentage) + '      %')
        try:
            media = eyed3.load(song)
            artist = str(media.tag.artist).title()
            title = str(media.tag.title).title()
            if media.tag.artist:
                if media.tag.title:
                    triple.append([media.info.time_secs, title, artist])
                if not media.tag.title:
                    triple.append([media.info.time_secs, song, artist])
            if not media.tag.artist:
                if media.tag.title:
                    triple.append([media.info.time_secs, title, "unknown"])
                if not media.tag.title:
                    triple.append([media.info.time_secs, song, "unknown"])
        except:
            pass
    print('100      %')
    triple.sort(reverse=True)
    LISTSONGDUR.grid(column=0, row=0, columnspan=2)
    for element in triple:
        duree = datetime.timedelta(seconds=int(element[0]))
        LISTSONGDUR.insert("end", " {} :  {} : {}".format(element[1], element[2], duree))
    BUTTON_CSV.grid(column=0, row=1)
    ButtonDB.grid(column=1, row=1)
    RESULTWINDOW.grid(column=0, row=3)
####################################
def ODDSONG():
    LISTSONGDUR.pack_forget()
    LISTDUR.pack_forget()
    ODDSONGS.pack_forget()
    SONGSFREQ.pack_forget()
    LISTFREQ.pack_forget()
    DOUBLELIST.pack_forget()
    listsing = fillmp()
    odd = []
    for song in listsing:
        os.system('cls' if os.name == 'nt' else 'clear')
        percentage = int(listsing.index(song)*100/len(listsing))
        print(str(percentage) + '      %')
        try:
            media = eyed3.load(song)
            artist = str(media.tag.artist).title()
            title = str(media.tag.title)
            if not artist:
                odd.append('No artist:  ' + song)
            if not title:
                odd.append('no title: ' + song)
        except:
            pass
    ODDSONGS.pack()
    total = len(odd)
    if int(total) == 0:
        ODDSONGS.insert("end", "All your mp3 have artist and title")
        ODDSONGS.insert("end", "CONGRATULATIONS")
    else:
        for element in odd:
            ODDSONGS.insert("end", element)

MAINWINDOW = Tk()
MAINWINDOW.title("MP3 Analyzer")
WIDTHSCREEN = MAINWINDOW.winfo_screenwidth()
HEIGHTSCREEN = MAINWINDOW.winfo_screenheight()
HEIGHTBT = int(HEIGHTSCREEN/600)
WIDTHBT = int(WIDTHSCREEN/60)
DIR = StringVar()
MYFRAME = LabelFrame(MAINWINDOW, fg=fg, bg=bg, width=int(WIDTHSCREEN/27))
LABELDIR = Button(MYFRAME, text="select Directory", width='15', fg=fg, bg=bg, command=browse)
DIRECTORY = Entry(MYFRAME, textvariable=DIR, width='30', fg=bg, bg=fg)
LABELDIR.grid(column=0, row=0)
DIRECTORY.grid(column=1, row=0, columnspan=2)
DIRECTORY.focus_set()

MYFRAME.grid(column=0, row=0)
MYFRAME2 = LabelFrame(MAINWINDOW, fg=fg, bg=bg)
SINGERFREQ = Button(MYFRAME2, text="Frequency of singer", width=WIDTHBT, height=HEIGHTBT, fg=fg, bg=bg, command=freq)
SINGERFREQ.grid(column=0, row=1)
SINGERDUR = Button(MYFRAME2, text="Duration of singer", width=WIDTHBT, height=HEIGHTBT, fg=fg, bg=bg, command=dur)
SINGERDUR.grid(column=1, row=1)
SONGDUR = Button(MYFRAME2, text="Duration of song", width=WIDTHBT, height=HEIGHTBT, fg=fg, bg=bg, command=SONGDUR)
SONGDUR.grid(column=0, row=2)
ODDSONG = Button(MYFRAME2, text="song without title or artist", width=WIDTHBT, height=HEIGHTBT, fg=fg, bg=bg, command=ODDSONG)
ODDSONG.grid(column=1, row=2)
SONGFREQ = Button(MYFRAME2, text="Song title occurence", width=WIDTHBT, height=HEIGHTBT, fg=fg, bg=bg, command=SONGFREQ)
SONGFREQ.grid(column=0, row=3)
DOUBLE = Button(MYFRAME2, text="Multiple occurence artist/song", width=WIDTHBT, height=HEIGHTBT, fg=fg, bg=bg, command=songsingfreq)
DOUBLE.grid(column=1, row=3)
CHANGECOLOR = Button(MYFRAME2, text="Change COLORS...", fg=bg, bg=fg, command=changecolor)
CHANGECOLOR.grid(column=0, row=4)
QUIT = Button(MYFRAME2, text="QUIT", fg=bg, bg=fg, command=MAINWINDOW.destroy)
QUIT.grid(column=1, row=4)
MYFRAME2.grid(column=0, row=1)
RESULTWINDOW = Frame(MAINWINDOW, height='10')
CHANGECOLORS = Frame(MAINWINDOW)

LISTFREQ = Listbox(RESULTWINDOW, width=int(WIDTHSCREEN/27), height=int(HEIGHTSCREEN/70), bg='white', fg='black')

LISTDUR = Listbox(RESULTWINDOW, width=int(WIDTHSCREEN/27), height=int(HEIGHTSCREEN/70), bg='white', fg='black')

LISTSONGDUR = Listbox(RESULTWINDOW, width=int(WIDTHSCREEN/27), height=int(HEIGHTSCREEN/70), bg='white', fg='black')

ODDSONGS = Listbox(RESULTWINDOW, width=int(WIDTHSCREEN/27), height=int(HEIGHTSCREEN/70), bg='white', fg='black')

SONGSFREQ = Listbox(RESULTWINDOW, width=int(WIDTHSCREEN/27), height=int(HEIGHTSCREEN/70), bg='white', fg='black')

DOUBLELIST = Listbox(RESULTWINDOW, width=int(WIDTHSCREEN/27), height=int(HEIGHTSCREEN/70), bg='white', fg='black')


MAINWINDOW.mainloop()
